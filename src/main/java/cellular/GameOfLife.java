package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	int numRows;
	int numColumns;
	CellState nextState;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		numRows = currentGeneration.numRows();
		numColumns = currentGeneration.numColumns();
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return numRows;
	}

	@Override
	public int numberOfColumns() {
		return numColumns;
	}

	@Override
	public CellState getCellState(int row, int col) {
		CellState state = currentGeneration.get(row, col);
		return state;
	}

	@Override // Updates the state of the cell according to the rules of the automaton
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row <= numRows; row++) {
			for (int col = 0; col <= numColumns; col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// number of neighbors that are alive around the cell
		int aliveN = this.countNeighbors(row, col, CellState.ALIVE);

		if (this.getCellState(row, col) == CellState.ALIVE) {
			if (aliveN <= 1) {
				nextState = CellState.DEAD;
			}
			if (aliveN == 2 || aliveN == 3) {
				nextState = CellState.ALIVE;
			}
			if (aliveN > 3) {
				nextState = CellState.DEAD;
			}
		}
		if (this.getCellState(row, col) == CellState.DEAD) {
			if (aliveN == 3) {
				nextState = CellState.ALIVE;
			}
		}
		return nextState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	public int countNeighbors(int row, int col, CellState state) {
		int stateCounter = 0;
		// Initiate row-loop
		for (int rowI = (row - 1); rowI <= (row + 1); rowI++) {
			if (rowI < 0 || rowI > this.numberOfRows()) {
				continue;
			}
			for (int colI = (col - 1); colI <= (col + 1); colI++) {
				if ((colI < 0) || (colI > currentGeneration.numColumns()) || (rowI == row && colI == col)) {
					continue;
				}
				if (currentGeneration.get(rowI, colI) == state) {
					stateCounter ++;
				}
			}
		}
		return stateCounter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
