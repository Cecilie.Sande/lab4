package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    int numRows;
    int numColumns;
    IGrid currentGeneration;
    CellState nextState;

    // Create constructor
    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		numRows = currentGeneration.numRows();
		numColumns = currentGeneration.numColumns();
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        CellState state = currentGeneration.get(row, column);
		return state;
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row <= numRows; row++) {
			for (int col = 0; col <= numColumns; col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

    @Override
	public CellState getNextCell(int row, int col) {
		// number of neighbors that are alive around the cell
		int aliveN = this.countNeighbors(row, col, CellState.ALIVE);
        CellState currentCellState = this.getCellState(row, col);

        if (currentCellState == CellState.ALIVE) {
            nextState = CellState.DYING;
        }
        if (currentCellState == CellState.DYING) {
            nextState = CellState.DEAD;
        }
        if (currentCellState == CellState.DEAD) {
            if (aliveN == 2) {
                nextState = CellState.ALIVE;
            }
            else {
                nextState = CellState.DEAD;
            }
        }
		return nextState;
	}

    @Override
    public int numberOfRows() {
        return numRows;
    }

    @Override
    public int numberOfColumns() {
        return numColumns;
    }

    public int countNeighbors(int row, int col, CellState state) {
		int stateCounter = 0;
		// Initiate row-loop
		for (int rowI = (row - 1); rowI <= (row + 1); rowI++) {
			if (rowI < 0 || rowI > this.numberOfRows()) {
				continue;
			}
			for (int colI = (col - 1); colI <= (col + 1); colI++) {
				if ((colI < 0) || (colI > currentGeneration.numColumns()) || (rowI == row && colI == col)) {
					continue;
				}
				if (currentGeneration.get(rowI, colI) == state) {
					stateCounter ++;
				}
			}
		}
		return stateCounter;
	}

    @Override
    public IGrid getGrid() {
		return currentGeneration;
	}

}