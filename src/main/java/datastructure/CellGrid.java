package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    int gridArea;
    CellState initialState;
    // Create an array whose elements are of type CellState
    CellState[][] grid;
    
    // Constructor
    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        gridArea = rows * columns;
        grid = new CellState[rows][columns];
        // Fill grid with CellState parameter
        for (CellState[] row : grid){
            Arrays.fill(row, initialState);
        }

	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > rows || column < 0 || column > columns) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row > rows || column < 0 || column > columns) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.columns, this.initialState);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                newGrid.set(row, column, this.get(row, column));
            }
        }
        return newGrid;
    }
    
}
